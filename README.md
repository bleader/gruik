# Gruik, your news source

_Gruik_ is an _IRC_ bot written in Go that fetches _RSS_ feeds and prints them on a
chosen channel.  
It has no database and focuses on news freshness, it will store a `ringsize` number of news
which rotate as news come in.  
_Gruik_ supports _x-posting_ meaning that it can join other channels than the main one so you
can post curated news using their hash, so the _x-post_ channels only receive a controlled
number of news.

* `maxnews` is the maximum number of news fetched from an _origin_
* `maxage` is the maximum news age, no news older than this will be fetched
* `frequency` is the refresh frequency
* `ringsize` is the number of news to keep on memory, disk, and rotate on

## Configuration

Here's a sample `config.yaml` file:

```yaml
irc:
  server: irc.libera.chat
  nick: Gruik
  channel: "#myfeedz"
  xchannels:           # optional, channels where you´d like to xpost
    - "#otherchannel"  #
  password: piggypiggy # optional
  debug: false         # ditto
  port: 6667           # ditto
  delay: 2s            # ditto, delay between 2 lines to avoid flood
  colors:              # ditto
    origin: pink       # ditto
    news: bold         # ditto
    link: lightblue    # ditto
    hash: gray         # ditto
  ops:                 # ditto
    - MrFoo            # ditto
    - MrsBar           # ditto

feeds:
  urls:
    - https://news.ycombinator.com/rss
    - https://rss.slashdot.org/Slashdot/slashdotMain
    - https://www.osnews.com/feed/
    - https://lwn.net/headlines/rss
    - https://www.phoronix.com/rss.php
    - https://lobste.rs/rss
    - https://undeadly.org/cgi?action=rss
    - https://www.freebsd.org/news/feed.xml
    - https://feeds.fireside.fm/bsdnow/rss
    - https://www.dragonflydigest.com/feed
    - https://newsletter.nixers.net/feed.xml
    - https://vermaden.wordpress.com/feed/
    - https://www.journalduhacker.net/rss
    - https://linuxfr.org/news.atom
    - https://rss.nytimes.com/services/xml/rss/nyt/World.xml
    - https://www.nextinpact.com/rss/news.xml
    - https://www.freebsd.org/security/feed.xml
    - https://www.discoverbsd.com/feeds/posts/default
    - https://netbsd.fi/atom.xml
  maxnews: 10   # optional
  maxage: 1h    # optional
  frequency: 5m # optional
  ringsize: 100 # optional, number of news to rotate on
```

## Usage

```shell
$ ./Gruik
```

## IRC commands

For everybody:

* `!lsfeeds` lists available RSS feeds, can be asked privately
* `!xpost <news hash>` posts news with hash `<news hash>` to `xchannels`
* `!latest <number> [origin]` will post `number` of latest news from optional `origin` in a query message

For ops:

* `!addfeed <RSS feed URL>`, adds an RSS feed URL
* `!rmfeed <id>`, removes feed by number (given by `!lsfeeds`)
* `!die`, kills the bot

An alternate configuration file can be given as a parameter.
